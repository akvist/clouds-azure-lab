import azure.durable_functions as df
from itertools import chain

def orchestrator_function(context: df.DurableOrchestrationContext):
    data = yield context.call_activity('GetInputDataFn', None)

    mapTasks = []
    for d in data:
        mapTasks.append(context.call_activity('Mapper', d))

    mapResult = yield context.task_all(mapTasks)
    mapResultFlattened = list(chain.from_iterable(mapResult))
    shuffleResult = yield context.call_activity('Shuffler', mapResultFlattened)

    reduceTasks = []
    for s in shuffleResult:
        reduceTasks.append(context.call_activity('Reducer', s))
    reduceResult = yield context.task_all(reduceTasks)

    return reduceResult

main = df.Orchestrator.create(orchestrator_function)