def main(tokenized: list) -> list:
    shuffler = {}

    for pair in tokenized:
        key = pair[0]

        if key not in shuffler:
            shuffler[key] = []
        shuffler[key].append(1)
    
    shuffled = shuffler.items() # convert the dictionary into a list of pairs
    return list(shuffled)
