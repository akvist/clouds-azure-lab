def main(pair: tuple) -> list:
    line = pair[1]
    words = line.split()
    tokenized = []

    for word in words:
        tokenized.append((word, 1))

    return tokenized
