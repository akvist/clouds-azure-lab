from azure.storage.blob import BlobServiceClient

def main(name: str) -> list:
    all_lines = []
    container_name = "mrinput"
    connectStr = "DefaultEndpointsProtocol=https;AccountName=durablefunctionsstorage1;AccountKey=66YE0lsKrmq+x78SazOTF1WBdcHyEfwS8bBz8jMh2rKxshgKfZYToqiapPGYr4GId/KVpr7qQ6W9+AStgZmMjg==;EndpointSuffix=core.windows.net"
    
    blob_service_client = BlobServiceClient.from_connection_string(connectStr)
    container_client = blob_service_client.get_container_client(container_name)

    blob_list = container_client.list_blobs()
    line_index = 0
    for blob in blob_list:
        blob_data = container_client.download_blob(blob.name).readall().decode('utf-8').split('\n')
        for line in blob_data:
            line_index += 1
            all_lines.append((line_index, line.strip()))
    return all_lines
