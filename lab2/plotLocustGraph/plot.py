import matplotlib.pyplot as plt
import pandas as pd

csvEnding = "_stats_history.csv"
integral = pd.read_csv("integral" + csvEnding)
integralY = integral["Requests/s"] - integral["Failures/s"]

scaleset = pd.read_csv("scaleset" + csvEnding)
scalesetY = scaleset["Requests/s"] - scaleset["Failures/s"]

webapp = pd.read_csv("webappscaling" + csvEnding)
webappY = webapp["Requests/s"] - webapp["Failures/s"]

function = pd.read_csv("functionscaling" + csvEnding)
functionY = function["Requests/s"] - function["Failures/s"]

plt.subplots(figsize=(10, 5))
plt.tight_layout()
plt.plot(integralY, label='(i) Numericalintegral')
plt.plot(scalesetY, label='(ii) Scaleset')
plt.plot(webappY, label='(iii) Webapp')
plt.plot(functionY, label='(iv) Autoscale Function')
plt.ylabel("Successfull Requests/s")
plt.xlabel("Time (s)")
plt.legend()
plt.show()
