import logging
import azure.functions as func
import json
from calc.integral import calculate_integrals


def main(req: func.HttpRequest) -> func.HttpResponse:
    
    lower = float(req.route_params.get("lower"))
    upper = float(req.route_params.get("upper"))

    result = calculate_integrals(float(lower), float(upper))
    return func.HttpResponse(json.dumps(result), mimetype='application/json', status_code=200)
